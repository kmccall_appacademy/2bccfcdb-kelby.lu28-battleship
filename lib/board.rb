class Board

  def self.default_grid
    Array.new(10) { Array.new(10, nil) }
  end

  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def count
    @grid.flatten.count(:s)
  end

  def display
    (0...@grid.length).each do |row|
      puts "#{row}: #{@grid[row]}"
    end
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def empty?(*pos)
    row, col = pos[0]
    return true if !pos.empty? && @grid[row][col].nil?

    @grid.flatten.all? { |pos| pos.nil? }
  end

  def full?
    @grid.flatten.none? { |pos| pos.nil? }
  end

  def place_random_ship

    raise "Error: board is full" if self.full?

    unless self.full?
      row = rand(@grid.length)
      col = rand(@grid.length)
      @grid[row][col] = :s
    end

  end

  def won?
    @grid.flatten.none? { |pos| pos == :s }
  end

end
