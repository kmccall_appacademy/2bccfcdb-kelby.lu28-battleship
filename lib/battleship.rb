require_relative 'player'
require_relative 'board'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = Player.new, board = Board.new)
    @player = player
    @board = board
    @current_player = @player
  end

  def attack(pos)
    if @board[pos] == :s
      puts "You just destroyed a ship!"
    end

    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def display_status
    @board.display
    puts "There are #{count} ships remaining."
  end

  def play_turn
    move = @current_player.get_play
    attack(move)
    puts "----------------"
    display_status
  end

  def play
    5.times do
      @board.place_random_ship
    end

    until game_over?
      play_turn
    end
  end

end

if __FILE__ == $PROGRAM_NAME
  x = BattleshipGame.new
  x.play
end
