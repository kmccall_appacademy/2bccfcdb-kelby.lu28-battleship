class ComputerPlayer

  def initialize(name = "Bob")
    @name = name
  end

  def get_play
    row = rand(10)
    col = rand(10)

    [row, col]
  end

end
